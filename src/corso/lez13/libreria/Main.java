package corso.lez13.libreria;

import java.sql.SQLException;

import corso.lez13.libreria.model.Libro;
import corso.lez13.libreria.model.dao.LibroDAO;

public class Main {

	public static void main(String[] args) {

		LibroDAO daoLibro = new LibroDAO(); 
		
		try {
			
			Libro libCercato = daoLibro.findById(2);
			System.out.println(libCercato.toString());
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
	}

}
