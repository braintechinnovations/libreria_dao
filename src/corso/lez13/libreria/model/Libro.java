package corso.lez13.libreria.model;

public class Libro {

	private int libroId;
	private String titolo;
	private Autore autoreRif;	// Indirizzo ad un oggetto di tipo autore ;)
	
	public Libro() {
		
	}
	
	public Libro(int libroId, String titolo, Autore autoreRif) {
		this.libroId = libroId;
		this.titolo = titolo;
		this.autoreRif = autoreRif;
	}
	
	public int getLibroId() {
		return libroId;
	}
	public void setLibroId(int libroId) {
		this.libroId = libroId;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public Autore getAutoreRif() {
		return autoreRif;
	}
	public void setAutoreRif(Autore autoreRif) {
		this.autoreRif = autoreRif;
	}

	@Override
	public String toString() {
		return "Libro [libroId=" + libroId + ", titolo=" + titolo + ", autoreRif=" + autoreRif + "]";
	}
	
}
