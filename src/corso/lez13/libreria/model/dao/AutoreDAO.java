package corso.lez13.libreria.model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import corso.lez13.libreria.model.Autore;
import corso.lez13.libreria.model.db.ConnettoreDB;

public class AutoreDAO implements Dao<Autore> {

	@Override
	public void create(Autore t) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<Autore> findAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Autore findById(int id) throws SQLException {

		ConnettoreDB singleton = ConnettoreDB.getInstance();
		Connection conn = singleton.getConnection();
		
		String sql = "SELECT autoreID, nome FROM autore WHERE autoreID = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
		ps.setInt(1, id);
		
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			Autore temp = new Autore();
			temp.setAutoreId(rs.getInt(1));
			temp.setNome(rs.getString(2));
			
			return temp;
		}
		
		return null;
	}

	@Override
	public boolean update(Autore t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Autore t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int id) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
