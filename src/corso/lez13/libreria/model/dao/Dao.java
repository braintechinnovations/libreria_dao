package corso.lez13.libreria.model.dao;

import java.sql.SQLException;
import java.util.ArrayList;

public interface Dao<T> {

	void create(T t) throws SQLException;
	
	ArrayList<T> findAll() throws SQLException;
	
	T findById(int id) throws SQLException;
	
	boolean update(T t) throws SQLException;
	
	boolean delete(T t) throws SQLException;
	
	boolean delete(int id) throws SQLException;	//Posso farlo perch� le due firme sono diverse sullo stesso metodo!
	
}
