package corso.lez13.libreria.model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import corso.lez13.libreria.model.Autore;
import corso.lez13.libreria.model.Libro;
import corso.lez13.libreria.model.db.ConnettoreDB;

public class LibroDAO implements Dao<Libro>{

	@Override
	public void create(Libro t) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<Libro> findAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Libro findById(int id) throws SQLException {

		ConnettoreDB singleton = ConnettoreDB.getInstance();
		Connection conn = singleton.getConnection();
		
		String sql = "SELECT libroID, titolo, autoreRIF FROM libro WHERE libroID = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sql);
		ps.setInt(1, id);
		
		AutoreDAO daoAutore = new AutoreDAO();
		
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			Libro temp = new Libro();
			temp.setLibroId(rs.getInt(1));
			temp.setTitolo(rs.getString(2));
			temp.setAutoreRif(daoAutore.findById(rs.getInt(3)));
			
			return temp;
		}
		
		return null;
		
		
	}

	@Override
	public boolean update(Libro t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Libro t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(int id) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
