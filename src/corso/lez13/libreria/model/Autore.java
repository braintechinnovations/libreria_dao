package corso.lez13.libreria.model;

public class Autore {

	private int autoreId;
	private String nome;
	
	public Autore() {
		
	}
	
	public Autore(int autoreId, String nome) {
		this.autoreId = autoreId;
		this.nome = nome;
	}
	
	public int getAutoreId() {
		return autoreId;
	}
	public void setAutoreId(int autoreId) {
		this.autoreId = autoreId;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Autore [autoreId=" + autoreId + ", nome=" + nome + "]";
	}
	
}
