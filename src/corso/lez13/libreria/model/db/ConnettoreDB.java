package corso.lez13.libreria.model.db;

import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class ConnettoreDB {

	private static ConnettoreDB ogg_connettore;
	private Connection conn;	//Ci posso accedere solo se l'oggetto � stato creato, evito di dichiarlo Static cos� non ci posso accedere se il connettore non � stato creato!
	
	public static ConnettoreDB getInstance() {
		if(ogg_connettore == null) {
			ogg_connettore = new ConnettoreDB();
		}
		
		return ogg_connettore;
	}
	
	public Connection getConnection() throws SQLException {
		
		if(conn == null) {
			MysqlDataSource dataSource = new MysqlDataSource();
			dataSource.setServerName("localhost");	//Oppure 127.0.0.1
			dataSource.setPortNumber(3306);
			dataSource.setUser("root");
			dataSource.setPassword("toor");
			dataSource.setDatabaseName("libreria");
			dataSource.setUseSSL(false);
			
			conn = (Connection) dataSource.getConnection();
		}
		
		return conn;
	}
	
	
	
}
