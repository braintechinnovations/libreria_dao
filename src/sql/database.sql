DROP DATABASE IF EXISTS libreria;
CREATE DATABASE libreria;
USE libreria;

CREATE TABLE autore (
	autoreID INTEGER NOT NULL AUTO_INCREMENT,
    nome VARCHAR(250),
    PRIMARY KEY(autoreID)
);

CREATE TABLE libro (
	libroID INTEGER NOT NULL AUTO_INCREMENT,
    titolo VARCHAR(250),
    autoreRIF INTEGER NOT NULL,		-- Obbligo ad inserire PER FORZA l'autore alla creazione del libro
    PRIMARY KEY(libroID),
    FOREIGN KEY(autoreRIF) REFERENCES autore(autoreID) ON DELETE CASCADE
);

INSERT INTO autore(nome) VALUES 
("Giggione"), ("Pescone"), ("Fragolone");
SELECT * FROM autore;

INSERT INTO libro(titolo, autoreRIF) VALUES
("Il libro pi� bello del mondo", 1),
("Il pesco pi� pesco del mondo", 2),
("Alcuni non sanno chi sono", 1),
("Certe notti, il kebab � caldo!", 3);
SELECT * FROM libro;

SELECT libro.titolo, autore.nome
    FROM libro
	INNER JOIN autore ON libro.autoreRIF = autore.autoreID;
